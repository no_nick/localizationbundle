<?php

namespace Mustang\LocalizationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Mustang\LocalizationBundle\Entity\Zipcode;
use Mustang\LocalizationBundle\Form\ZipcodeType;

/**
 * Zipcode controller.
 *
 * @Route("/admin/zip-code")
 */
class ZipcodeController extends Controller {

    /**
     * Lists all Zipcode entities.
     *
     * @Route("/", name="zipcodes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MustangLocalizationBundle:Zipcode')->findAll();

        $deleteForms = array();
        foreach ($entities as $entity) {
            $deleteForms[$entity->getId()] = $this->createDeleteForm($entity->getId())->createView();
        }

        return array(
            'entities' => $entities,
            'deleteForms' => $deleteForms
        );
    }

    /**
     * Creates a new Zipcode entity.
     *
     * @Route("/", name="zipcodes_create")
     * @Method("POST")
     * @Template("MustangLocalizationBundle:Zipcode:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Zipcode();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getCity() == null) {
                $this->get('session')->getFlashBag()->add('success', 'Dodaj w pierwszej kolejności miasto.');
                return $this->redirect($this->generateUrl('cities'));
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodawanie kodu pocztowego zakończone powodzeniem.');
            return $this->redirect($this->generateUrl('zipcodes_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Zipcode entity.
     *
     * @param Zipcode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm(Zipcode $entity) {
        $form = $this->createForm(new ZipcodeType(), $entity, array(
            'action' => $this->generateUrl('zipcodes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Dodaj', 'attr' => array('class' => 'buttonS bGreen send')));

        return $form;
    }

    /**
     * Displays a form to create a new Zipcode entity.
     *
     * @Route("/new", name="zipcodes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Zipcode();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Zipcode entity.
     *
     * @Route("/{id}", name="zipcodes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MustangLocalizationBundle:Zipcode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zipcode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Zipcode entity.
     *
     * @Route("/{id}/edit", name="zipcodes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MustangLocalizationBundle:Zipcode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zipcode entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Zipcode entity.
     *
     * @param Zipcode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm(Zipcode $entity) {
        $form = $this->createForm(new ZipcodeType(), $entity, array(
            'action' => $this->generateUrl('zipcodes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Edytuj', 'attr' => array('class' => 'buttonS bGreen')));

        return $form;
    }

    /**
     * Edits an existing Zipcode entity.
     *
     * @Route("/{id}", name="zipcodes_update")
     * @Method("PUT")
     * @Template("MustangLocalizationBundle:Zipcode:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MustangLocalizationBundle:Zipcode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zipcode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid() && $entity->getCity() != null) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edycja kodu pocztowego zakończona powodzeniem.');

            return $this->redirect($this->generateUrl('zipcodes_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Zipcode entity.
     *
     * @Route("/{id}", name="zipcodes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MustangLocalizationBundle:Zipcode')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Zipcode entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Usuwanie kodu pocztowego zakończone powodzeniem.');
        }

        return $this->redirect($this->generateUrl('zipcodes'));
    }

    /**
     * Creates a form to delete a Zipcode entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('zipcodes_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
