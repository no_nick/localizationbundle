<?php

namespace Mustang\LocalizationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Mustang\LocalizationBundle\Entity\Province;
use Mustang\LocalizationBundle\Form\ProvinceType;

/**
 * Province controller.
 *
 * @Route("/admin/provinces")
 */
class ProvinceController extends Controller {

    /**
     * Lists all Province entities.
     *
     * @Route("/", name="provinces")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MustangLocalizationBundle:Province')->findAll();

        $deleteForms = array();
        foreach ($entities as $entity) {
            $deleteForms[$entity->getId()] = $this->createDeleteForm($entity->getId())->createView();
        }

        return array(
            'entities' => $entities,
            'deleteForms' => $deleteForms
        );
    }

    /**
     * Creates a new Province entity.
     *
     * @Route("/", name="provinces_create")
     * @Method("POST")
     * @Template("MustangLocalizationBundle:Province:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Province();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getCountry() == null) {
                $this->get('session')->getFlashBag()->add('success', 'Dodaj w pierwszej kolejności kraj.');
                return $this->redirect($this->generateUrl('countries'));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodawanie województwa zakończone powodzeniem.');
            return $this->redirect($this->generateUrl('provinces_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Province entity.
     *
     * @param Province $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm(Province $entity) {
        $form = $this->createForm(new ProvinceType(), $entity, array(
            'action' => $this->generateUrl('provinces_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Dodaj', 'attr' => array('class' => 'buttonS bGreen send')));

        return $form;
    }

    /**
     * Displays a form to create a new Province entity.
     *
     * @Route("/new", name="provinces_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Province();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Province entity.
     *
     * @Route("/{id}", name="provinces_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MustangLocalizationBundle:Province')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Province entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Province entity.
     *
     * @Route("/{id}/edit", name="provinces_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MustangLocalizationBundle:Province')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Province entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Province entity.
     *
     * @param Province $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm(Province $entity) {
        $form = $this->createForm(new ProvinceType(), $entity, array(
            'action' => $this->generateUrl('provinces_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Edytuj', 'attr' => array('class' => 'buttonS bGreen')));

        return $form;
    }

    /**
     * Edits an existing Province entity.
     *
     * @Route("/{id}", name="provinces_update")
     * @Method("PUT")
     * @Template("MustangLocalizationBundle:Province:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MustangLocalizationBundle:Province')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Province entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid() && $entity->getCountry() != null) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edycja województwa zakończona powodzeniem.');

            return $this->redirect($this->generateUrl('provinces_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Province entity.
     *
     * @Route("/{id}", name="provinces_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MustangLocalizationBundle:Province')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Province entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Usuwanie województwa zakończone powodzeniem.');
        }

        return $this->redirect($this->generateUrl('provinces'));
    }

    /**
     * Creates a form to delete a Province entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('provinces_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
