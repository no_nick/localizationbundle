<?php

namespace Mustang\Component\Settings\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class BaseSetting {
    
    /**
     * @ORM\Column(name="setting_name", type="string", length=255)
     */
    private $settingName;

    /**
     * @ORM\Column(name="setting_description", type="string", length=255)
     */
    private $settingDescription;

    /**
     * @ORM\Column(name="setting_value", type="string", length=255)
     */
    private $settingValue;

    public function setSettingName($settingName) {
        $this->settingName = $settingName;

        return $this;
    }

    public function getSettingName() {
        return $this->settingName;
    }

    public function setSettingDescription($settingDescription) {
        $this->settingDescription = $settingDescription;

        return $this;
    }

    public function getSettingDescription() {
        return $this->settingDescription;
    }

    public function setSettingValue($settingValue) {
        $this->settingValue = $settingValue;

        return $this;
    }

    public function getSettingValue() {
        return $this->settingValue;
    }
}
