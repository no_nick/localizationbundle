<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Component\Resource\Model\Traits;

/**
 * Description of SluggableNamesTrait
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
trait SluggableNamesTrait {
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
