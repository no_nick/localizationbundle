<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Component\Resource\Model;

/**
 * Description of SlugAwareInterface
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface SlugAwareInterface {
    
    /**
     * Get permalink/slug.
     *
     * @return string
     */
    public function getSlug();

    /**
     * Set permalink/slug.
     *
     * @param string $slug
     */
    public function setSlug($slug);
}
