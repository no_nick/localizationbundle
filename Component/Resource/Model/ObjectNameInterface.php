<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Component\Resource\Model;

/**
 * Description of ObjectNameInterface
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface ObjectNameInterface {
    
    /**
     * @param string $name
     * 
     * @return $this
     */
    public function setName($name);
    
    /**
     * return string
     */
    public function getName();
}
