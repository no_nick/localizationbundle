<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Component\Resource\Model;

/**
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface AddressInterface {
    
    /**
     * @return string
     */
    public function getAddress();

    /**
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address);
}
