<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Component\Resource\Model;

/**
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface RentPriceInterface {
    
    /**
     * @return float
     */
    public function getPricePerHourNetto();
    
    /**
     * @param float $pricePerHourNetto
     * 
     * @return $this
     */
    public function setPricePerHourNetto($pricePerHourNetto);
}
