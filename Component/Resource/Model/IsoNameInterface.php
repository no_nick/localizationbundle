<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Component\Resource\Model;

/**
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface IsoNameInterface {
    
    /**
     * @return string
     */
    public function getIsoName();

    /**
     * @param string $isoName
     *
     * @return $this
     */
    public function setIsoName($isoName);
}
