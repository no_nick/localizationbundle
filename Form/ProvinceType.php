<?php

namespace Mustang\LocalizationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProvinceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Nazwa województwa'))
            ->add('isoName', null, array('label' => 'Kod ISO województwa'))
            ->add('country', 'entity', array(
                'label' => 'Kraj',
                'class' => 'MustangLocalizationBundle:Country',
                'property' => 'name',
                'attr'  => array('class' => 'styled')
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mustang\LocalizationBundle\Entity\Province'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mustang_localizationbundle_province';
    }
}
