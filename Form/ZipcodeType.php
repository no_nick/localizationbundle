<?php

namespace Mustang\LocalizationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ZipcodeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, array('label' => 'Kod pocztowy [00-000]'))
            ->add('city', 'entity', array(
                'label' => 'Miasto',
                'class' => 'MustangLocalizationBundle:City',
                'property' => 'label',
                'attr'  => array('class' => 'styled'),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mustang\LocalizationBundle\Entity\Zipcode'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mustang_localizationbundle_zipcode';
    }
}
