<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Entity;

use Mustang\LocalizationBundle\Component\Resource\Model\SlugAwareInterface;

/**
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface ZipCodeInterface extends
    SlugAwareInterface
{
    
    /**
     * @return mixed
     */
    public function getId();
    
    /**
     * return string
     */
    public function getCode();
    
    /**
     * 
     * @param string $code
     * 
     * @return $this
     */
    public function setCode($code);
    
    /**
     * 
     * @param CityInterface $city
     */
    public function setCity(CityInterface $city = null);
    
    /**
     * @return CityInterface $city
     */
    public function getCity();
}
