<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Entity\Interfaces;

use Mustang\LocalizationBundle\Entity\CityInterface;
use Mustang\LocalizationBundle\Entity\ProvinceInterface;
use Mustang\LocalizationBundle\Entity\ZipCodeInterface;
use Mustang\LocalizationBundle\Component\Resource\Model\AddressInterface;

/**
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface AddressableInterface extends
    AddressInterface
{
    /**
     * @return ZipCodeInterface describes postal code for localization
     */
    public function getZipcode();
    
    /**
     * @param ZipCodeInterface $zipcode
     * 
     * @return $this
     */
    public function setZipcode(ZipCodeInterface $zipcode);
    
    /**
     * @return CityInterface $city
     */
    public function getCity();
    
    /**
     * @param CityInterface $city
     * 
     * @return $this
     */
    public function setCity(CityInterface $city);
    
    /**
     * @return ProvinceInterface $province
     */
    public function getProvince();
    
    /**
     * @param ProvinceInterface $province
     * 
     * @return $this
     */
    public function setProvince(ProvinceInterface $province);
}
