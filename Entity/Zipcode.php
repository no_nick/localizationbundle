<?php

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * Zipcode
 *
 * @MappedSuperclass
 */
abstract class Zipcode implements ZipCodeInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @Gedmo\Slug(fields={"code"})
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @Assert\Regex("/^\d{2}-\d{3}$/")
     * @ORM\Column(name="code", type="string", length=255)
     */
    protected $code;

    /**
     *
     * @ORM\ManyToOne(targetEntity="City", inversedBy="zipcodes")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    protected $city;

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    public function __toString() {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * {@inheritdoc}
     */
    public function setCode($code) {
        $this->code = $code;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function setCity(CityInterface $city = null) {
        $this->city = $city;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCity() {
        return $this->city;
    }

}
