<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Mustang\LocalizationBundle\Component\Resource\Model\SlugAwareInterface;
use Mustang\LocalizationBundle\Component\Resource\Model\ObjectNameInterface;
use Mustang\LocalizationBundle\Component\Resource\Model\IsoNameInterface;

/**
 * Description of CountryInterface
 *
 * @author Mustang Marketing
 */
interface CountryInterface extends
    SlugAwareInterface,
    ObjectNameInterface,
    IsoNameInterface
{
    
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return Collection|ProvinceInterface[]
     */
    public function getProvinces();

    /**
     * @param Collection $provinces
     */
    public function setProvinces(Collection $provinces);

    /**
     * @return bool
     */
    public function hasProvinces();

    /**
     * @param ProvinceInterface $province
     */
    public function addProvince(ProvinceInterface $province);

    /**
     * @param ProvinceInterface $province
     */
    public function removeProvince(ProvinceInterface $province);

    /**
     * @param ProvinceInterface $province
     *
     * @return bool
     */
    public function hasProvince(ProvinceInterface $province);
}
