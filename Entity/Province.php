<?php

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * Province
 *
 * @MappedSuperclass
 */
abstract class Province implements ProvinceInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="iso_name", type="string", length=255)
     */
    protected $isoName;

    /**
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="province")
     */
    protected $cities;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="provinces")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    protected $country;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->cities = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setIsoName($isoName) {
        $this->isoName = $isoName;

        return $this;
    }

    /**
     * {@inheritdoc} 
     */
    public function getIsoName() {
        return $this->isoName;
    }

    /**
     * {@inheritdoc}
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * {@inheritdoc}
     */
    public function addCity(CityInterface $cities) {
        $this->cities[] = $cities;
        $cities->setProvince($this);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeCity(CityInterface $cities) {
        $this->cities->removeElement($cities);
    }

    /**
     * {@inheritdoc}
     */
    public function getCities() {
        return $this->cities;
    }

    /**
     * {@inheritdoc}
     */
    public function setCities(Collection $cities) {
        $this->cities = $cities;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasCities() {
        return !$this->cities->isEmpty();
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasCity(CityInterface $city) {
        return $this->cities->contains($city);
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCountry(CountryInterface $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry() {
        return $this->country;
    }
}
