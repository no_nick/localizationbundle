<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Mustang\LocalizationBundle\Component\Resource\Model\SlugAwareInterface;
use Mustang\LocalizationBundle\Component\Resource\Model\ObjectNameInterface;
use Mustang\LocalizationBundle\Component\Resource\Model\IsoNameInterface;

/**
 * Description of ProvinceInterface
 *
 * @author Mustang Marketing
 */
interface ProvinceInterface extends
    SlugAwareInterface,
    ObjectNameInterface,
    IsoNameInterface
{
    
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return CountryInterface
     */
    public function getCountry();

    /**
     * @param CountryInterface $country
     *
     * @return $this
     */
    public function setCountry(CountryInterface $country = null);
    
    /**
     * @return Collection|CityInterface[]
     */
    public function getCities();
    
    /**
     * @param Collection $cities
     */
    public function setCities(Collection $cities);
    
    /**
     * @return bool
     */
    public function hasCities();
    
    /**
     * @param CityInterface $city
     */
    public function addCity(CityInterface $city);
    
    /**
     * @param CityInterface $city
     */
    public function removeCity(CityInterface $city);
    
    /**
     * @param CityInterface $city
     * 
     * @return bool
     */
    public function hasCity(CityInterface $city);
}
