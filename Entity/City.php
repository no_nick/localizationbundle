<?php

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * City
 *
 * @MappedSuperclass
 */
abstract class City implements CityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="namename", type="string", length=255)
     */
    protected $name;

    /**
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="Zipcode", mappedBy="city")
     */
    protected $zipcodes;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="cities")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    protected $province;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->zipcodes = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    public function getLabel() {
        $out = $this->name;
        $out .= ($this->getProvince() != NULL) ? ' ('.$this->getProvince()->getName().')' : '';
        return $out;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * {@inheritdoc}
     */
    public function addZipcode(ZipCodeInterface $zipcodes) {
        $this->zipcodes[] = $zipcodes;
        $zipcodes->setCity($this);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeZipcode(ZipCodeInterface $zipcodes) {
        $this->zipcodes->removeElement($zipcodes);
    }

    /**
     * {@inheritdoc}
     */
    public function getZipcodes() {
        return $this->zipcodes;
    }

    /**
     * {@inheritdoc}
     */
    public function setProvince(ProvinceInterface $province) {
        $this->province = $province;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProvince() {
        return $this->province;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setZipcodes(Collection $zipcodes) {
        $this->zipcodes = $zipcodes;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasZipcode(ZipCodeInterface $zipcode) {
        return $this->zipcodes->contains($zipcode);
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasZipcodes() {
        return !$this->zipcodes->isEmpty();
    }
}
