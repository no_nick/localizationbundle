<?php

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * Country
 *
 * @MappedSuperclass
 */
abstract class Country implements CountryInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="iso_name", type="string", length=255)
     */
    protected $isoName;

    /**
     * @ORM\OneToMany(targetEntity="Province", mappedBy="country")
     */
    protected $provinces;

    /**
     * Constructor
     */
    public function __construct() {
        $this->provinces = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setIsoName($isoName) {
        $this->isoName = $isoName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getIsoName() {
        return $this->isoName;
    }

    /**
     * {@inheritdoc}
     */
    public function addProvince(ProvinceInterface $province) {
        $this->provinces[] = $provinces;
        $provinces->setCountry($this);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasProvince(ProvinceInterface $province) {
        return $this->provinces->contains($province);
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasProvinces()
    {
        return !$this->provinces->isEmpty();
    }
    
    /**
     * {@interitdoc}
     */
    public function removeProvince(ProvinceInterface $provinces) {
        $this->provinces->removeElement($provinces);
    }

    /**
     * {@inheritdoc}
     */
    public function getProvinces() {
        return $this->provinces;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setProvinces(Collection $provinces)
    {
        $this->provinces = $provinces;

        return $this;
    }
}
