<?php

/*
 * This file is part of the Edudesk package.
 * 
 * (c) Adam Terepora
 */

namespace Mustang\LocalizationBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Mustang\LocalizationBundle\Component\Resource\Model\SlugAwareInterface;
use Mustang\LocalizationBundle\Component\Resource\Model\ObjectNameInterface;

/**
 * Description of CityInterface
 *
 * @author Adam Terepora <a.terepora@mustang-marketing.pl>
 */
interface CityInterface extends 
    SlugAwareInterface,
    ObjectNameInterface
{
    
    /**
     * @return mixed
     */
    public function getId();
    
    /**
     * @param ProvinceInterface $province
     * 
     * @return $this
     */
    public function setProvince(ProvinceInterface $province);
    
    /**
     * @return ProvinceInterface
     */
    public function getProvince();
    
    /**
     * 
     * @param Collection $zipcodes
     */
    public function setZipcodes(Collection $zipcodes);
    
    /**
     * @return Collection|ZipCodeInterface[]
     */
    public function getZipcodes();
    
    /**
     * 
     * @param ZipCodeInterface $zipcode
     */
    public function addZipcode(ZipCodeInterface $zipcode);
    
    /**
     * 
     * @param ZipCodeInterface $zipcode
     */
    public function removeZipcode(ZipCodeInterface $zipcode);
    
    /**
     * 
     * @param ZipCodeInterface $zipcode
     * 
     * @return bool
     */
    public function hasZipcode(ZipCodeInterface $zipcode);
    
    /**
     * @return bool
     */
    public function hasZipcodes();
}
